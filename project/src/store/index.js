import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import {public_key} from '../movies'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    movies: [],
    options: {
      page: 0,
    },
  },
  
  mutations: {
 
    SET_MOVIES(state, movies) {
      state.movies = movies
      movies.forEach(item => {
        movies.push(item)
        console.log(item)
      });
    },

    NEW_MOVIE(state, newMovie){
      console.log(newMovie)
      let movies = state.movies.concat(newMovie);
      state.movies = movies
    },

    DELETE_MOVIE(state, idMovie){
      let movies = state.movies.filter( m => m.id != idMovie)
      state.movies = movies;
    },


    SET_PAGINATION(state, options){
      const page = options
    }
      
  },

  actions: {
  
    async getMovies({commit}){
      let response = await axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=${public_key}&language=en-US`)
      .then((response) => {
        commit('SET_MOVIES', response.data.results)
        commit('SET_PAGINATION', response.data.page)
      });
     },
     
      async addMovie({commit}, newMovie){
        console.log(newMovie)
       commit('NEW_MOVIE', newMovie)
     },

     async deleteM({commit}, idMovie){
        console.log(idMovie)
        commit('DELETE_MOVIE', idMovie)
   },


   },
 })

